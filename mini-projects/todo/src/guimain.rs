extern crate gtk;
extern crate pango;

/**
 * Module gui
 * Handles creating the main gui of the application.
 * 
 * functions:
 * 		+ start -> void
 * 		- formatted_date -> String
 * 		- on_btn_add_clicked -> void: &ListBox, String
 * 		- on_check_toggled -> void: &ListBoxRow 
 */
pub mod gui {

	// uses
	use chrono;

	use gtk::prelude::*;

	use gtk::{
		Align, Application, ApplicationWindowBuilder, Button, Box, 
		BoxBuilder, CheckButtonBuilder, EntryBuilder, EntryBuffer, 
		Justification, LabelBuilder, ListBox, ListBoxRow,
		ListBoxRowBuilder, Orientation, ScrolledWindowBuilder
	};
	
	// functions
	
	/// Initialises and styles default Widgets of the main gui
	pub fn start(app: &Application) {
		// Data
		let buffer = EntryBuffer::new(Some(""));
		
		// Builders
		let window_builder 
				= ApplicationWindowBuilder::new()
						.application(app);
						
		let box_main_builder 
				= BoxBuilder::new()
						.margin(12)
						.spacing(12);
						
		let entry_builder 
				= EntryBuilder::new()
						.hexpand(true)
						.buffer(&buffer);
						
		let scroll_builder 
				= ScrolledWindowBuilder::new()
						.vexpand(true);
		
		// Widgets
		let window = window_builder.build();
        window.set_title("To Do");
        window.set_default_size(450, 600);
		
		let box_main = box_main_builder.build();
		box_main.set_orientation(Orientation::Vertical);
		
		let hbox = Box::new(Orientation::Horizontal, 6);

		let entry = entry_builder.build();
		
        let button = Button::new_with_label("     Add Task     ");
        
		let scroll = scroll_builder.build();
		
        let listbox = ListBox::new();
		
		// Add Widgets to Containers
		window.add(&box_main);
		
		box_main.add(&hbox);
        box_main.add(&scroll);
        
		hbox.add(&entry);
        hbox.add(&button);
        
        scroll.add(&listbox);
        
		// Update Widgets
		window.show_all();
		
		// Connect Signals							 
        button.connect_clicked(move|_| {
           on_btn_add_clicked(&listbox, buffer.get_text());
           
           buffer.set_text("");
           
           window.show_all();
        });
	}

	/// Returns the current date and time as a String
	fn formatted_date() -> String {
		let date = chrono::offset::Local::now().to_string();
		let date_slice = &date[ .. 19];
		date_slice.to_string()
	}

	// signal functions
	
	/// Adds a listboxrow to the listbox passed in as a parameter
	fn on_btn_add_clicked(listbox: &ListBox, content: String) {
		// Data
		let text = content;
		let date = formatted_date();
		
		// Builders
		let row_builder 
				= ListBoxRowBuilder::new();
		
		let hbox_row_builder
				= BoxBuilder::new()
						.margin(12);

		let vbox_row_builder 
				= BoxBuilder::new()
						.hexpand(true)
						.margin(12);
		
		let lbl_date_builder 
				= LabelBuilder::new()
						.label(&date)
						.justify(Justification::Left)
						.halign(Align::Start);
						
		let lbl_text_builder
				= LabelBuilder::new()
						.label(&text)
						.wrap(true)
						.wrap_mode(pango::WrapMode::WordChar);
						
		let ch_btn_builder = CheckButtonBuilder::new();
		
					
		// Widgets
		let row = row_builder.build();
		
		let hbox_row = hbox_row_builder.build();
		
		let vbox_row = vbox_row_builder.build();
		vbox_row.set_orientation(Orientation::Vertical);
		
		let lbl_date = lbl_date_builder.build();
		
		let lbl_text = lbl_text_builder.build();
		
		let ch_btn = ch_btn_builder.build();
		
		
		// Add Widgets to containers
		listbox.add(&row);
		
		row.add(&hbox_row);
		
		hbox_row.add(&vbox_row);
		
		vbox_row.add(&lbl_date);
		vbox_row.add(&lbl_text);
		
		hbox_row.add(&ch_btn);
		
		// Connect Signals
		ch_btn.connect_toggled(move|_| {
			on_check_toggled(&row);
		});
	}

	/// destroys the row passed in
	fn on_check_toggled(row: &ListBoxRow) {
		row.destroy()
	}
}
