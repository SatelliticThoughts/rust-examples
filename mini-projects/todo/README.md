# To Do

A very simple to do application, to practice Rust and Gtk.

## Setting up

After cloning/downloading the project run

```
cargo build # to build the project
cargo run # to run the project
```

## Usage

Write in the entry field and click the Add Item button,
to add an item to the list.

Toggle the checkbutton to remove it.

![](img/application.png)

## License
[MIT](https://choosealicense.com/licenses/mit/)
