/*
 * Problem:
 * 		The prime factors of 13195 are 5, 7, 13 and 29.
 * 		What is the largest prime factor of the number 600851475143 ?
 * 
 * Origin: Project Euler
 * Link: https://projecteuler.net/problem=3
 * 
 */

 
fn is_prime(x: i64) -> bool { 
	if x == 2 { return true }
	_is_prime(x, 2) 
} 
fn _is_prime(x: i64, i: i64) -> bool {
	if x % i == 0 { return false }
	if i >= x / 2 { return true } 
	_is_prime(x, i + 1)
}


fn next_prime(x: i64) -> i64 {
	let i = x + 1;
	if is_prime(i) { return i }
	next_prime(i)
}


fn largest_prime_factor(x: i64) -> i64 { 
	_largest_prime_factor(x, next_prime(1)) 
}
fn _largest_prime_factor(x: i64, next: i64) -> i64 {
	if x % next == 0 {
		let result = x / next;
		if is_prime(result) { return result }
		return _largest_prime_factor(result, next_prime(next))
	}
	_largest_prime_factor(x, next_prime(next))
}
 
 
fn main() {
	let a: i64 = largest_prime_factor(13195);
	println!("Answer: {}", a);
	
	let b: i64 = largest_prime_factor(600851475143);
	println!("Answer: {}", b);
}
