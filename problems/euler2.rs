/*
 * Problem:
 *      Each new term in the Fibonacci sequence is generated
 *      by adding the previous two terms. By starting with 1 and 2,
 *      the first 10 terms will be:
 *          1, 2, 3, 5, 8, 13, 21, 34, 55, 89, ...
 *
 *      By considering the terms in the Fibonacci sequence whose values
 *      do not exceed four million, find the sum of the even-valued terms.
 *
 *  Origin: Project Euler
 *  Link: https://projecteuler.net/problem=2
 *
 */


fn update_values(x: &mut i32, y: &mut i32) {
    *x = *x + *y;
    *y = *x - *y;
}


fn fibonacci_sequence(n: i32) -> i32 {
    let mut prev: i32 = 1; 
    let mut curr: i32 = 1;

    let mut total: i32 = 0;

    while curr < n {
        total += curr * (curr % 2 == 0) as i32;
        update_values(&mut curr, &mut prev);
    }
    total
}


fn main() {
    println!("Answer: {}", fibonacci_sequence(4000000));
}
