/*
 * Problem:
 *      If we list all the natural numbers below 10
 *      that are multiples of 3 or 5, we get 3, 5, 6 and 9.
 *      Find the sum of all the multiples of 3 or 5 below 1000.
 *
 *  Origin: Project Euler
 *  Link: https://projecteuler.net/problem=1
 *
 */


fn is_multiple(x: i32) -> i32 {
    (x % 3 == 0 || x % 5 == 0) as i32
}


fn total(n: i32) -> i32 {
    let mut total: i32 = 0;
    for i in 1..n {
        total += i * is_multiple(i);
    }
    total
}


fn main() {
    println!("Answer for 10: {}", total(10));
    println!("Answer for 1000: {}", total(1000));
}

