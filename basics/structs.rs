struct Person {
    name: String,
    age: i32,
}

fn main() {
    let mut p = Person{name: "".to_string(), age: 0};
    p.name = "luke".to_string();
    p.age = 206;

    println!("Name: {}, Age: {}", p.name, p.age);
}
