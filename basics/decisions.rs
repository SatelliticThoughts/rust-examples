fn main() {
    // Basic if statement
    if true {
        println!("This will show");
    }

    // Basic if/else statement
    if false {
        println!("This will NOT show");
    } else {
        println!("This will show");
    }

    // Basic if/else if/else statement
    if false {
        println!("This will NOT show");
    } else if true {
        println!("This will show");
    }  else {
        println!("This will NOT show");
    }

    // &&, and
    if true && true {
        println!("This will show");
    }

    if true && false {
        println!("This will NOT show");
    }

    if false && false {
        println!("This will NOT show");
    }

    // ||, or
    if true || true {
        println!("This will show");
    }

    if true || false {
        println!("This will show");
    }

    if false || false {
        println!("This will NOT show");
    }

    let x: i32 = 8;
    let y: i32 = 4;

    // Equals
    if x == y {
        println!("This will show if x equals y");
    }

    // Not Equal
    if x != y {
        println!("This will show if x does not equal y");
    }

    // Greater Than
    if x > y {
        println!("This will show if x is bigger than y");
    }

    // Less than
    if x < y {
        println!("This will show if x is smaller than y");
    }

    // Setting variable
    let _bigger = if x > y { x } else { y };
    println!("{} is the bigger number", _bigger);
}
