fn main() {
    let x : i32 = 4;
    let y : i32 = 2;

    // Integers
    
    println!("Integer Calculations: x = {}, y = {}", x, y);

    println!("x + y = {}", x + y); // Addition
    println!("x - y = {}", x - y); // Subtraction
    println!("x * y = {}", x * y); // Multiplication
    println!("x / y = {}", x / y); // Division
    println!("x % y = {}", x % y); // Modulus


    let a : f32 = 4.1;
    let b : f32 = 1.8;

    // Floats

    println!("\nFloating point Calculations: a = {}, b = {}", a, b);

    println!("a + b = {}", a + b); // Addition
    println!("a - b = {}", a - b); // Subtraction
    println!("a * b = {}", a * b); // Multiplication
    println!("a / b = {}", a / b); // Division
    println!("a % b = {}", a % b); // Modulus

    // Strings
    let mut hello = String::from("hello");
    println!("{}", hello);

    hello.push_str(" world ");
    println!("{}", hello);

    let _hello = &hello[2 .. 5];
    println!("{}", _hello); // Substring

    // Arrays
    let mut array: [i32; 3] = [0; 3];
    
    for i in 0..3 {
        array[i] = (i as i32) * 10;
    }

    println!("{}", array[2]);
}

