fn main() {
    let mut num: i32 = 0;

    // Infinite loop
    loop {
        num += 1;
        if num > 5 {
            break;
        }
    }

    let mut running: bool = true;

    // while loop
    while running {
        num -= 1;
        if num < 1 {
            running = false;
        }
    }

    // for loop
    for i in 0..13 {
        println!("14 * {} = {}", i, (i as i32) * 14);
    }

    let array:[&str; 3] = ["jim", "tim", "vim"];

    for item in &array {
        println!("{}", item);
    }
}
