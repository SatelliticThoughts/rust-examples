fn main() {
    // Hello World example
    println!("Hello World!");

    // Printing variable example
    let text = "This is a test";
    println!("{}", text);

    // Printing multiple variables
    let name = "luke";
    let age = 300;

    println!("My name is {} and i am {} years old", name, age);
}

