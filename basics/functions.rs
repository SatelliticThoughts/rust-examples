fn main() {
    say_hello();
    println!("{}", get_word());
    double_number(40);
    println!("{}", add_numbers(3, 2));
}

fn say_hello() {
    println!("Hello")
}

fn get_word() -> String {
    "word".to_string()
}

fn double_number(x: i32) {
    println!("{}", x * 2);
}

fn add_numbers(x: i32, y: i32) -> i32 {
    x + y
}


