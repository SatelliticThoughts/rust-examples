# rust-examples

rust-examples contains code to demonstrate various things in Rust.

## Set up

After cloning or downloading, single rust files should be compiled with rustc and executed.

```
rustc "filename.rs" # Compile file
./"filename" # Execute file
```

Multi file projects are created using cargo.

```
cargo build # Build project
cargo run # Run Project
```

## License
[MIT](https://choosealicense.com/licenses/mit/)

